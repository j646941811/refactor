package comp5911m.refactor;


import java.util.ArrayList;
import java.util.List;


public class Customer {
  private String name;
  private List<Rental> rentals;

  public Customer(String name) {
    this.name = name;
    rentals = new ArrayList<>();
  }

  public String getName() {
    return name;
  }

  public void addRental(Rental rental) {
    rentals.add(rental);
  }

  public String statement() {
    int totalAmount = 0;
    int frequentRenterPoints = 0;

    StringBuilder result = new StringBuilder();
    result.append("Rental record for ");
    result.append(getName());
    result.append(":\n");

    // Determine amounts for each rental
    for (Rental rental : rentals) {
      // Each rental earns a frequent renter point...
      ++frequentRenterPoints;

      // ...plus a bonus for at least 3 days rental of a new model
//      if (rental.getCar().getPriceCode() == Car.NEW_MODEL && rental.getDaysRented() >= 3) {
//        ++frequentRenterPoints;
//      }
//      frequentRenterPoints += getFrequentRenterPoints(rental);
//      frequentRenterPoints += rental.getFrequentRenterPoints();

        // Show figures for this rental
      result.append("- ");
      result.append(rental.getCar().getModel());
      result.append(": ");
      result.append(rental.getDaysRented());
      result.append(" days, £");
      result.append(rental.getCharge());
      result.append("\n");

//      totalAmount += rental.getCharge();
    }

    frequentRenterPoints += getfrequentRenterPoints();
    totalAmount += getTotalCharge();
    // Add footer lines
    result.append("Amount owed is £");
    result.append(totalAmount);
    result.append("\nYou earned ");
    result.append(frequentRenterPoints);
    result.append(" frequent renter points\n");

    return result.toString();
  }

  public void htmlStatement() {
    String htmlstr = statement();
    StringBuilder result = new StringBuilder();
    result.append(" <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
    result.append("<html>");
    result.append("<head>");
    result.append("<title>Demo</title>");
    result.append("</head>");
    result.append("<body>");
    result.append(htmlstr);
    result.append("</body>");
    result.append("</html>");
    System.out.println(result.toString());
  }

  private int getTotalCharge() {
    int result = 0;
    for (Rental rental : rentals) {
      result += rental.getCharge();
    }
    return result;
  }

  private int getfrequentRenterPoints() {
    int result = 0;
    for (Rental rental : rentals) {
      result += rental.getFrequentRenterPoints();
    }
    return result;
  }

//  private int getFrequentRenterPoints(Rental rental) {
//    if (rental.getCar().getPriceCode() == Car.NEW_MODEL && rental.getDaysRented() >= 3) {
//      return 1;
//    }
//    else {
//      return 0;
//    }
//  }

//  private int getFrequentRenterPoints(Rental rental) {
//    return rental.getFrequentRenterPoints();
//  }
}
