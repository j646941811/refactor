package comp5911m.refactor;


public class Rental {
  private Car car;
  private int daysRented;

  public Rental(Car car, int daysRented) {
    this.car = car;
    this.daysRented = daysRented;
  }

  public Car getCar() {
    return car;
  }

  public int getDaysRented() {
    return daysRented;
  }

  public int getCharge() {
    int result = 0;
    switch (getCar().getPriceCode()) {
      case Car.STANDARD:
        result += 30 * getDaysRented();
        break;
      case Car.LUXURY:
        result += 50 * getDaysRented();
        break;
      case Car.NEW_MODEL:
        result += 40 * getDaysRented();
        break;
    }
    return result;
  }

  public int getFrequentRenterPoints(){
    if (getCar().getPriceCode() == Car.NEW_MODEL && getDaysRented() >= 3) {
      return 1;
    }
    else {
      return 0;
    }
  }
}
